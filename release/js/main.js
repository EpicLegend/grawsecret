$(".carousel__arrows_prev").on("click", function () {
	$("#carouselWelcome").carousel("prev");
	$("#carouselWelcomeText").carousel("prev");
});
$(".carousel__arrows_next").on("click", function () {
	$("#carouselWelcome").carousel("next");
	$("#carouselWelcomeText").carousel("next");
});




if ( $("form").is(".form__numbers") ) {
	$(".number__minus").on("click", function (e) {
		e.preventDefault(); 
		inputNumber("minus", this);
	});
	$(".number__plus").on("click", function (e) {
		e.preventDefault(); 
		inputNumber("plus", this);
	});
}

function inputNumber(operant, context) {
	var obj = $(context).parent().find("input[type='number']");
	if ( operant == "plus" ) {
		$(obj).val( parseInt($(obj).val()) + 1 );
	} else if ( operant == "minus" ) {
		if ( $(obj).attr("min") < $(obj).val() )
			$(obj).val( parseInt($(obj).val()) - 1 );
	}
	$(obj).parent().find(".form__numbers_val").html( $(obj).val() );
}

$(".btn_filter").on("click", function () {
	$(".new__items__filtr").toggleClass("active");

	if ( $(this).hasClass("btn_filter_active") ) {
		$(this).removeClass("btn_filter_active");
	} else {
		$(this).addClass("btn_filter_active");
	}
});

$(".btn_not").on("click", function (e) {
	e.preventDefault();
	e.stopPropagation();
});