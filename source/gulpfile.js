'use strict';

var gulp 	= require('gulp'),
    sass 	= require('gulp-sass'),
   	watch 	= require('gulp-watch'),
   	pug 	= require('gulp-pug'),
   	rename  = require('gulp-rename');

gulp.task('sass', function () {
	console.log("complete sass");
   return gulp.src('sass/style.scss')
   		  .pipe(sass())
          .pipe(gulp.dest('../release/css/'));
});

gulp.task('pug', function () {
	console.log("complete pug");
	return gulp.src('html/main.pug')
		   .pipe(pug({pretty: true}))
		   .pipe(rename('index.html'))
		   .pipe(gulp.dest("../release"));
});


gulp.task('watch', function () {
   gulp.watch('sass/style.scss', gulp.series('sass'));
   gulp.watch('html/main.pug', gulp.series('pug'));
});


gulp.task('default', gulp.series(['sass', 'pug'], 'watch'));








gulp.task('pugcategory', function () {
  console.log("complete pug");
  return gulp.src('html/category.pug')
       .pipe(pug({pretty: true}))
       .pipe(gulp.dest("../release"));
});
gulp.task('watch', function () {
   gulp.watch('sass/style.scss', gulp.series('sass'));
   gulp.watch('html/category.pug', gulp.series('pugcategory'));
});
gulp.task('category', gulp.series(['sass', 'pugcategory'], 'watch'));





gulp.task('pugonly', function () {
  console.log("complete pug");
  return gulp.src('html/only.pug')
       .pipe(pug({pretty: true}))
       .pipe(gulp.dest("../release"));
});
gulp.task('watch', function () {
   gulp.watch('sass/style.scss', gulp.series('sass'));
   gulp.watch('html/only.pug', gulp.series('pugonly'));
});
gulp.task('only', gulp.series(['sass', 'pugonly'], 'watch'));





gulp.task('pugorder', function () {
  console.log("complete pug");
  return gulp.src('html/order.pug')
       .pipe(pug({pretty: true}))
       .pipe(gulp.dest("../release"));
});
gulp.task('watch', function () {
   gulp.watch('sass/style.scss', gulp.series('sass'));
   gulp.watch('html/order.pug', gulp.series('pugorder'));
});
gulp.task('order', gulp.series(['sass', 'pugorder'], 'watch'));





gulp.task('pugfinal', function () {
  console.log("complete pug");
  return gulp.src('html/final.pug')
       .pipe(pug({pretty: true}))
       .pipe(gulp.dest("../release"));
});
gulp.task('watch', function () {
   gulp.watch('sass/style.scss', gulp.series('sass'));
   gulp.watch('html/final.pug', gulp.series('pugfinal'));
});
gulp.task('final', gulp.series(['sass', 'pugfinal'], 'watch'));
